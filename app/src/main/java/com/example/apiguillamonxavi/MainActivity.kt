package com.example.apiguillamonxavi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.apiguillamonxavi.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var firstButton: Button
    lateinit var secondButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        firstButton = findViewById(R.id.button)
        secondButton = findViewById(R.id.button2)


        firstButton.setOnClickListener {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, FirstFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }

        secondButton.setOnClickListener {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, SecondFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }


    }
}